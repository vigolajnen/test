// var map;
// function initMap() {
//   map = new google.maps.Map(document.getElementById("map"), {
//     center: { lat: 55.7465072, lng: 37.5341161 },
//     zoom: 16
//   });
// }


var marker;

function initMap() {
  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: { lat: 55.746596, lng: 37.537445 } // 55.7473047,37.5369322
  });

  var image =
    "../../img/icons/icon-14.svg";

  marker = new google.maps.Marker({
    map: map,
    // icon: image,
    title: "Москва г, наб.Пресненская, д.10, пом.18",
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: { lat: 55.746596, lng: 37.537445 }
  });
  marker.addListener("click", toggleBounce);
}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
