(function () {
  var e = $(".scrollToTop")
    , t = $("body");
  $(window).scroll(function () {
    $(this).scrollTop() > 520 ? ($(".scrollToTop").fadeIn(),
      t.fadeIn()) : ($(".scrollToTop").fadeOut(),
        t.fadeOut())
  }),
    e.click(function (e) {
      return e.preventDefault(),
        $("html, body").animate({
          scrollTop: 0
        }, 800),
        !1
    })
})();