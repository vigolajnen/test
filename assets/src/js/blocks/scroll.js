// header-fixed
(function () {



// ScrollReveal().reveal(".block-news", { delay: 200 });
ScrollReveal().reveal(".block-projects", { delay: 200 });
ScrollReveal().reveal(".block-about__item", { delay: 200 });
ScrollReveal().reveal(".block-progress", { delay: 200 });
ScrollReveal().reveal(".partners", { delay: 200 });
ScrollReveal().reveal(".block-map", { delay: 200 });
ScrollReveal().reveal(".page__content", { delay: 200 });

})();

// header-fixed end
